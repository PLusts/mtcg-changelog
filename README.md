# Midgard TCG Info

## Feature Plans

### Confirmed
- Card collection sorting by user option.
- System to create custom packs for rewards.

### To be decided
- Display larger images on charactersheet popup.
- Display image of deprecated character sheets on popup.
- Sort cards by user.

## Known Bugs
- Bernard Wolcyzty is missing an Image

## Releases

### 1.1.3 (12.11.2024)
#### Features
- Added cardback for daemons
- Added cardback for hybrids
- Added cardback for humans
- Added 3 new basic cards
    - Yukio Omocha
    - Ina Inizia
    - Selania Robinson
- Update the image for Varis Diakon 

### 1.1.2 (01.11.2024)
#### Features
- Improved performance by reducing the image size of some assets
- Improved performance by using sprite sheets for character images
- Added 6 new basic cards
    - Azazel Dementrus
    - Elayne Kuritsa
    - Sakura Kamajiro
    - Gwendoline Muertos
    - Stix Yhara
    - Tiskani Mocaná

#### Fixes
- Fixed regeression issue for new users which caused showing errors after login/linking a discord user

### 1.1.1 (06.10.2024)
#### Fixes
- Resolved regression issue that prevented new users from accessing the dashboard after login.

### 1.1.0 (21.09.2024)
#### Features
- Added the marketplace.
    - Allows to list cards from the collection by double clicking them.
        - Listed cards will not show up in a user collection as long as they are listed
    - Allows to cancel offers from the marketplace by double clicking them within the market.
        - Upon cancelation of an offer the card returns to the users collection.
    - Contains a transaction history of bought and sold cards.
        - The transaction history icon will adjust to notify users about sold cards.
    - Allows to buy cards for Gummypoints.
- Added statistics on the dashboard.
    - Includes counts by rarity
    - Includes counts by pack
- The card border and the holo effect is now shown in the popup.
- Added 9 new basic cards
    - Ophelia Nihil
    - Puck Nihil
    - Seraphine Rose des Ombres
    - Hlökk Tyrdóttir
    - Skuld
    - Jasper McMan
    - Yuri Yamadara
    - Hachitobi Yamadara
    - Wakanu Yamadara

#### Fixes
- Fixed a bug that made the search case sensitive.
- Fixed a bug that caused the holo effect and the card count to display faulty upon changing the sorting order



### 1.0.3 (24.07.2024)
#### Features
- Added a new card back for the angel pack
- Added 17 new basic cards
    - Naomi Inazuma
    - Eleazar Nihil
    - Anastasia Borealis
    - Pavel Timur Kusnezow
    - Lloyd van Hoozer
    - Akrasiel Nihil
    - Alphonse Belviére
    - Bernard Wolcyzty
    - Abdul Zahask-Alim
    - Roger-Toreo Draknay
    - Vivian McScully
    - Morai Wakibane
    - Horatio Horot
    - Rosaria VII. von Rosenfeld
    - Evelynn Ripper
    - Makoto Uemina
    - Athene Mikotynos
- Updated 2 basic cards
    - Tethys Mythlock
    - Hex Hex

### 1.0.2 (01.05.2024)
#### Features
- Added a new banner after revealing a card that you do not own
- Added 5 new basic cards
    - Avery Ophelia Schuyler-Luu
    - Elliot Serberuss
    - Kasuto Shigaraki
    - Raymond Serberuss
    - Ryo Suzuki

### 1.0.1 (06.04.2024)
#### Features
- Added 5 new basic cards (Zugai Kotsu, Alexander Kellerman, Sebastian von Rosenberg, Stefanie Hildebrandt, Ninagi Kamishira)
- Updated Nidria Mamboa
- Added 10 new special cards

#### Fixes
- Added 1 missing rare card image
- Improved loading times for certain pages
- Rounded gummypoints in shop

###  1.0.0 (01.04.2024)
#### Features
- Card Shop
- Login Page
- Dashboard
- Card Overview 